import { createApp } from "vue";
import App from "./App.vue";
import PrimeVue from "primevue/config";

import "./style.css"; // глобальные стили
import "primevue/resources/themes/lara-light-indigo/theme.css"; // стили css фреймворка
import "primevue/resources/primevue.min.css";

const app = createApp(App);
app.use(PrimeVue).mount("#app");
